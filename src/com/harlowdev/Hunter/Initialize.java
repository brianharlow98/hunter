package com.harlowdev.Hunter;

import com.harlowdev.Hunter.PlayerTypes.Prey;
import com.harlowdev.Hunter.PlayerTypes.Predator;
import com.harlowdev.Hunter.PlayerTypes.Spectator;

public class Initialize
{
	static Prey prey = null;
	static Predator hunter = null;
	static Spectator spectator = null;
	
	public static void init(Prey preyList, Predator hunterList, Spectator specList)
	{
		prey = preyList;
		hunter = hunterList;
		spectator = specList;
	}

}
