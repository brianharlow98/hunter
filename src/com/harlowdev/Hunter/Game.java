package com.harlowdev.Hunter;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
//import org.bukkit.scheduler.BukkitTask;

import com.harlowdev.Hunter.PlayerTypes.Prey;
import com.harlowdev.Hunter.PlayerTypes.Predator;


public class Game
{
//	private static Prey prey = null;
//	private static Predator hunter = null;
//	private static Spectator spectator = null;
	
//	private static int numPrey = 0;
//	private static BukkitTask compassTask;
	private static int numHunter = 0;
	
	public static void setCompassTask(Prey prey, Predator hunter)
	{
		Bukkit.getLogger().info("[Hunter] Hunter Plugin setCompassTask called!");
		Bukkit.getScheduler().runTaskTimer(Hunter.plugin, new Runnable()
		{

			@Override
			public void run()
			{
				numHunter = hunter.hunterList.size();
				// For each hunter in the game
				for(int i = 0; i < numHunter; i ++)
				{
					// Update hunter's compass target to correct prey location
					//Bukkit.getLogger().info("[Hunter] Hunter Plugin setCompassTask loop for index " + i);
					Player p = hunter.getPlayer(i);
					p.setCompassTarget(prey.getPreyLocation(hunter.hunterTarget.get(i)));
				}
			}
			
		}, 0, 10);
	}
}
