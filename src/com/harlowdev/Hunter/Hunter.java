package com.harlowdev.Hunter;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.harlowdev.Hunter.PlayerTypes.Prey;
import com.harlowdev.Hunter.PlayerTypes.Predator;
import com.harlowdev.Hunter.PlayerTypes.Spectator;
import com.harlowdev.Hunter.Commands.CommandExec;
import com.harlowdev.Hunter.Events.CompassToggle;
import com.harlowdev.Hunter.Events.OnDeath;
import com.harlowdev.Hunter.Events.OnRespawn;

public class Hunter extends JavaPlugin {
		public static Plugin plugin;
		Prey prey = new Prey();
		Predator hunter = new Predator();
		Spectator spectator = new Spectator();
		
		@Override
		public void onEnable() {
			Bukkit.getLogger().info("[Hunter] Hunter Plugin Initializing");
			plugin = this;
			getCommand("hunter").setExecutor(new CommandExec(this, prey, hunter, spectator));
			//Initialize.init(prey, hunter, spectator);
			getServer().getPluginManager().registerEvents(new CompassToggle(prey, hunter), this);
			getServer().getPluginManager().registerEvents(new OnDeath(prey, hunter, spectator), this);
			getServer().getPluginManager().registerEvents(new OnRespawn(prey, hunter, spectator), this);
		}
		
		@Override
		public void onDisable(){
			
		}

}
