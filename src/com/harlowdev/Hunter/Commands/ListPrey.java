package com.harlowdev.Hunter.Commands;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.harlowdev.Hunter.PlayerTypes.Prey;

public class ListPrey implements SubCommand
{
	Prey prey = null;
	
	public ListPrey(Prey preyList)
	{
		prey = preyList;
	}
	
	@Override
	public boolean onCommand(Player player, String[] args)
	{
		return prey.listPrey(player);
	}

	@Override
	public String help(Player p)
	{
		return ChatColor.LIGHT_PURPLE 	+ "/hunter listPrey"
			 + ChatColor.GRAY 			+ " - Shows list of every prey still alive.";
	}

}
