package com.harlowdev.Hunter.Commands;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.harlowdev.Hunter.PlayerTypes.Predator;

public class ClearHunter implements SubCommand
{
	Predator hunter = null;
	
	public ClearHunter(Predator hunterList)
	{
		hunter = hunterList;
	}
	
	@Override
	public boolean onCommand(Player player, String[] args)
	{
		if(hunter.clearHunter())
		{
			player.sendMessage(ChatColor.GREEN + "Hunters successfully cleared!");
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public String help(Player p)
	{
		return ChatColor.LIGHT_PURPLE 	+ "/hunter clearHunter"
			 + ChatColor.GRAY 			+ " - Removes every user from the list of hunters.";
	}

}
