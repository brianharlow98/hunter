package com.harlowdev.Hunter.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.harlowdev.Hunter.PlayerTypes.Prey;

public class RemovePrey implements SubCommand
{
	Prey prey = null;
	
	public RemovePrey(Prey preyList)
	{
		prey = preyList;
	}

	@Override
	public boolean onCommand(Player player, String[] args)
	{
		// If player is removed from the list of prey
		if(prey.removePrey(Bukkit.getPlayer(args[0])))
		{
			player.sendMessage(ChatColor.GREEN + "User "
							 + ChatColor.LIGHT_PURPLE + args[0] + " "
							 + ChatColor.GREEN + "has been removed as a prey!");
		}
		else
		{
			player.sendMessage(ChatColor.RED + "User "
							 + ChatColor.GREEN + args[0] + " "
							 + ChatColor.RED + "was not set as prey.");
		}
		
		return true;
	}

	@Override
	public String help(Player p)
	{
		return ChatColor.LIGHT_PURPLE 	+ "/hunter removePrey [User]"
			 + ChatColor.GRAY 			+ " - Select a user to remove them as prey.";
	}
}
