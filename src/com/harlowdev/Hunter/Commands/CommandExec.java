package com.harlowdev.Hunter.Commands;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Vector;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import com.harlowdev.Hunter.PlayerTypes.Prey;
import com.harlowdev.Hunter.PlayerTypes.Predator;
import com.harlowdev.Hunter.PlayerTypes.Spectator;

public class CommandExec implements CommandExecutor
{
	private Prey prey = null;
	private Predator hunter = null;
	private Spectator spectator = null;
	private HashMap<String, SubCommand> commands;
	public static String pluginPrefix = ChatColor.DARK_GRAY 	+ "["
									  + ChatColor.LIGHT_PURPLE 	+ "Hunter" 
									  + ChatColor.DARK_GRAY 	+ "] " 
									  + ChatColor.GRAY;
	
	public void help(Player p)
	{
		p.sendMessage("/hunter <command> <args>");
		for(SubCommand v: commands.values())
		{
			p.sendMessage(ChatColor.AQUA + v.help(p));
		}
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd1, String commandLabel, String[] args)
	{
		String cmd = cmd1.getName();
		//PluginDescriptionFile pdfFile = plugin.getDescription();
		Player player = null;
		
		if (sender instanceof Player)
		{
			player = (Player) sender;
		}
		else
		{
			System.out.println(pluginPrefix + "Only users in the server are able to use commands.");
			return true;
		}

		if(cmd.equalsIgnoreCase("hunter"))
		{ 
			if(args == null || args.length < 1)
			{
				player.sendMessage(ChatColor.GRAY 	+ ""
							     + ChatColor.BOLD 	+ "Hunter"
							     + ChatColor.RESET);
				
				player.sendMessage(ChatColor.GRAY 			+ "Type "
								 + ChatColor.LIGHT_PURPLE 	+ "/hunter help "
								 + ChatColor.GRAY 			+ "for help");

				return true;
			}
			
			if(args[0].equalsIgnoreCase("help"))
			{
				help(player);
				return true;
			}
			
			String sub = args[0];
			Vector<String> l  = new Vector<String>();
			l.addAll(Arrays.asList(args));
			l.remove(0);
			args = (String[]) l.toArray(new String[0]);
			
			if(!commands.containsKey(sub))
			{
				player.sendMessage(pluginPrefix + ChatColor.RED + "Command doesn't exist.");
				
				player.sendMessage(ChatColor.GRAY 		+ "Type "
								 + ChatColor.DARK_RED 	+ "/hunter help "
								 + ChatColor.GRAY 		+ "for help.");
				
				return true;
			}
			
			try{

				commands.get(sub).onCommand(player, args);
			}catch(Exception e){e.printStackTrace(); player.sendMessage(pluginPrefix + ChatColor.RED + "Error!");
			
			player.sendMessage(ChatColor.GRAY 		+ "Type "
							 + ChatColor.DARK_RED	+ "/hunter help "
							 + ChatColor.GRAY 		+ "for help.");
			}
			
			return true;
		}
		
		return false;
	}
	
	public CommandExec(Plugin plugin, Prey preyList, Predator hunterList, Spectator specList)
	{
		prey = preyList;
		hunter = hunterList;
		spectator = specList;
		commands = new HashMap<String, SubCommand>();
		loadCommands();
	}

	private void loadCommands()
	{
		commands.put("addPrey",  		new AddPrey(prey, hunter));
		commands.put("removePrey", 		new RemovePrey(prey));
		commands.put("clearPrey", 		new ClearPrey(prey));
		commands.put("listPrey", 		new ListPrey(prey));
		
		commands.put("addHunter",  		new AddHunter(prey, hunter));
		commands.put("removeHunter", 	new RemoveHunter(hunter));
		commands.put("clearHunter", 	new ClearHunter(hunter));
		commands.put("listHunter", 		new ListHunter(hunter));
		
		commands.put("start", 			new Start(prey, hunter, spectator));
		commands.put("tracker", 		new Tracker(prey));
	}
	
	
}