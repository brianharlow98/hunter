package com.harlowdev.Hunter.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.harlowdev.Hunter.PlayerTypes.Prey;
import com.harlowdev.Hunter.PlayerTypes.Predator;

public class AddHunter implements SubCommand
{
	Prey prey = null;
	Predator hunter = null;
	
	public AddHunter(Prey preyList, Predator hunterList)
	{
		prey = preyList;
		hunter = hunterList;
	}
	
	@Override
	public boolean onCommand(Player player, String[] args)
	{
		Player newHunter = Bukkit.getPlayer(args[0]);
		
		// If selected player is present in the server
		if(newHunter != null)
		{
			// If selected player is already a Hunter
			if(prey.preyList.contains(newHunter))
			{
				player.sendMessage(ChatColor.RED + "Player "
								 + ChatColor.LIGHT_PURPLE + newHunter.getName() + " "
								 + ChatColor.RED + "is already a prey and cannot be added as a hunter!");
			}
			// If selected player is not already a prey
			else
			{
				if(!hunter.hunterList.contains(player))
				{
					hunter.hunterList.add(player);
					// Set the index of their compass target to 0
					hunter.hunterTarget.add(0);
					player.sendMessage(ChatColor.GREEN + "User "
							 + ChatColor.LIGHT_PURPLE + args[0] + " "
							 + ChatColor.GREEN + "has been added as a hunter!");
				}
				else
				{
					player.sendMessage(ChatColor.RED + "User "
									 + ChatColor.LIGHT_PURPLE + args[0] + " "
									 + ChatColor.RED + "is already set as a hunter!");
				}
			}
		}
		else
		{
			player.sendMessage(ChatColor.RED + "Invalid input! User is not present in the server.");
		}

		return true;
	}

	@Override
	public String help(Player p)
	{
		return ChatColor.LIGHT_PURPLE 	+ "/hunter addHunter [User]"
			 + ChatColor.GRAY 			+ " - Select a user present in the server to add as a hunter.";
	}

}
