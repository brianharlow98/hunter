package com.harlowdev.Hunter.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.harlowdev.Hunter.PlayerTypes.Prey;
import com.harlowdev.Hunter.PlayerTypes.Predator;

public class AddPrey implements SubCommand
{
	Prey prey = null;
	Predator hunter = null;
	
	public AddPrey(Prey preyList, Predator hunterList)
	{
		prey = preyList;
		hunter = hunterList;
	}
	
	@Override
	public boolean onCommand(Player player, String[] args)
	{
		Player newPrey = Bukkit.getPlayer(args[0]);
		
		// If selected player is present in the server
		if(newPrey != null)
		{
			// If selected player is already a hunter
			if(hunter.hunterList.contains(newPrey))
			{
				player.sendMessage(ChatColor.RED + "Player "
								 + ChatColor.LIGHT_PURPLE + newPrey.getName() + " "
								 + ChatColor.RED + "is already a hunter and cannot be added as a prey!");
			}
			// If selected player is not already a hunter
			else
			{
				if(!prey.preyList.contains(newPrey))
				{
					prey.preyList.add(newPrey);
					player.sendMessage(ChatColor.GREEN + "User "
									 + ChatColor.LIGHT_PURPLE + args[0] + " "
									 + ChatColor.GREEN + "has been added as a prey!");
				}
				else
				{
					player.sendMessage(ChatColor.RED + "User "
									 + ChatColor.GREEN + args[0] + " "
									 + ChatColor.RED + "is already set as prey.");
				}
			}

		}
		else
		{
			player.sendMessage(ChatColor.RED + "Invalid input! User is not present in the server.");
		}

		return true;
	}
	
	@Override
	public String help(Player player)
	{
		return ChatColor.LIGHT_PURPLE 	+ "/hunter addPrey [User]"
			 + ChatColor.GRAY 			+ " - Select a user present in the server as prey.";
	}
}
