package com.harlowdev.Hunter.Commands;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.harlowdev.Hunter.PlayerTypes.Prey;

public class ClearPrey implements SubCommand
{
	Prey prey = null;
	
	public ClearPrey(Prey preyList)
	{
		prey = preyList;
	}
	
	@Override
	public boolean onCommand(Player player, String[] args)
	{
		if(prey.clearPrey())
		{
			player.sendMessage(ChatColor.GREEN + "Prey successfully cleared!");
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public String help(Player p)
	{
		return ChatColor.LIGHT_PURPLE 	+ "/hunter clearPrey"
			 + ChatColor.GRAY 			+ " - Removes every user from the list of prey.";
	}
	
}
