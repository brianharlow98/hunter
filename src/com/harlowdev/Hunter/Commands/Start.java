package com.harlowdev.Hunter.Commands;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.harlowdev.Hunter.PlayerTypes.Prey;
import com.harlowdev.Hunter.PlayerTypes.Predator;
import com.harlowdev.Hunter.PlayerTypes.Spectator;

public class Start implements SubCommand
{
	private static Prey prey = null;
	private static Predator hunter = null;
	//private static Spectator spectator = null;
	
	private static int numPrey = 0;
	private static int numHunter = 0;
	
	public Start(Prey preyList, Predator hunterList, Spectator specList)
	{
		prey = preyList;
		hunter = hunterList;
		//spectator = specList;
	}
	
	@Override
	public boolean onCommand(Player player, String[] args) {
		numPrey = prey.getPreySize();
		numHunter = hunter.hunterList.size();
		
		// For each hunter
		for(int i = 0; i < numHunter; i++)
		{	
			// Clear the inventory of the player
			Player p = hunter.getPlayer(i);
			p.getInventory().clear();
			
			// Give the player a tracking compass
			int targetIndex = hunter.hunterTarget.get(i);
			p.sendMessage(ChatColor.GREEN + "Target is " + prey.getPlayer(targetIndex).getName());
			p.getInventory().addItem(new ItemStack(Material.COMPASS));
			p.setCompassTarget(prey.getPreyLocation(hunter.hunterTarget.get(i)));
			p.sendMessage("You have received a tracker!");
		}
		
		//Game.setCompassTask(prey, hunter);
		
		// For each prey
		for(int j = 0; j < numPrey; j++)
		{
			// Clear the inventory of the player
			Player p = prey.getPlayer(j);
			p.getInventory().clear();
		}
			
		return true;
	}

	@Override
	public String help(Player p) {
		return ChatColor.LIGHT_PURPLE 	+ "/hunter start"
			 + ChatColor.GRAY 			+ " - Starts the hunter game.";
	}

}
