package com.harlowdev.Hunter.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.harlowdev.Hunter.PlayerTypes.Predator;

public class RemoveHunter implements SubCommand
{
	Predator hunter = null;
	
	public RemoveHunter(Predator hunterList)
	{
		hunter = hunterList;
	}
	
	@Override
	public boolean onCommand(Player player, String[] args)
	{
		// If player is removed from the list of hunters
		if(hunter.removeHunter(Bukkit.getPlayer(args[0])))
		{
			player.sendMessage(ChatColor.GREEN + "User "
							 + ChatColor.LIGHT_PURPLE + args[0] + " "
							 + ChatColor.GREEN + "has been removed as a hunter!");
		}
		else
		{
			player.sendMessage(ChatColor.RED + "User "
							 + ChatColor.GREEN + args[0] + " "
							 + ChatColor.RED + "was not set as a hunter.");
		}
		
		return true;
	}

	@Override
	public String help(Player p)
	{
		return ChatColor.LIGHT_PURPLE 	+ "/hunter removeHunter [User]"
			 + ChatColor.GRAY 			+ " - Select a user to remove them as a hunter.";
	}

}
