package com.harlowdev.Hunter.Commands;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.harlowdev.Hunter.PlayerTypes.Predator;

public class ListHunter implements SubCommand
{
	Predator hunter = null;
	
	public ListHunter(Predator hunterList)
	{
		hunter = hunterList;
	}
	
	@Override
	public boolean onCommand(Player player, String[] args)
	{
		return hunter.listHunter(player);
	}

	@Override
	public String help(Player p)
	{
		return ChatColor.LIGHT_PURPLE 	+ "/hunter listHunter"
			 + ChatColor.GRAY 			+ " - Shows list of every hunter in the game.";
	}

}
