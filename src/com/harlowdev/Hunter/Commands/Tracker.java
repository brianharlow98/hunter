package com.harlowdev.Hunter.Commands;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.harlowdev.Hunter.PlayerTypes.Prey;

public class Tracker implements SubCommand
{
	Prey prey = null;
	
	public Tracker(Prey preyList)
	{
		prey = preyList;
	}
	
	@Override
	public boolean onCommand(Player player, String[] args)
	{
		player.getInventory().addItem(new ItemStack(Material.COMPASS));
		player.setCompassTarget(prey.getPreyLocation(0));
		player.sendMessage("You have received a tracker!");

		return true;
	}
	
	//@Override
	public String help(Player player)
	{
		return ChatColor.LIGHT_PURPLE 	+ "/hunter tracker"
			 + ChatColor.GRAY 			+ " - Gives a hunter a compass tracking the prey.";
	}
}
