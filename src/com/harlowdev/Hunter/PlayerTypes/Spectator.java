package com.harlowdev.Hunter.PlayerTypes;

import java.util.ArrayList;

import org.bukkit.entity.Player;

public class Spectator 
{
	public ArrayList<Player> specList = new ArrayList<Player>();
	
	public boolean addSpec(Player player)
	{
		specList.add(player);
		return true;
	}
	
	public boolean removeSpec(Player player)
	{
		int playerIndex = specList.indexOf(player);
		specList.remove(playerIndex);
		specList.trimToSize();
		return true;
	}
	
	public boolean clearSpec()
	{
		return specList.removeAll(specList);
	}
}
