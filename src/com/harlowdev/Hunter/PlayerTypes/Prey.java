package com.harlowdev.Hunter.PlayerTypes;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class Prey
{
	public ArrayList<Player> preyList = new ArrayList<Player>();
	
	public boolean removePrey(Player player)
	{	
		if(preyList.contains(player))
		{
			int playerIndex = preyList.indexOf(player);
			preyList.remove(playerIndex);
			preyList.trimToSize();
			if(preyList.contains(player))
			{
				return false;
			}

			return true;
		}

		return false;
	}
	
	public boolean clearPrey()
	{
		return preyList.removeAll(preyList);
	}
	
	public boolean listPrey(Player sender)
	{
		Player prey = null;
		
		int size = preyList.size();
		
		if(size > 0)
		{
			sender.sendMessage(ChatColor.GREEN 			+ "There are currently "
							 + ChatColor.LIGHT_PURPLE 	+ size + " "
							 + ChatColor.GREEN 			+ "prey set!");
			
			for(int i = 0; i < size; i++)
			{
				prey = preyList.get(i);
				sender.sendMessage(ChatColor.GREEN + prey.getName());
			}
			return true;
		}
		
		sender.sendMessage(ChatColor.RED + "There are no prey set!");
		return false;
	}
	
	public int getPreySize()
	{
		return preyList.size();
	}
	
	public Player getPlayer(int i)
	{
		return preyList.get(i);
	}
	
	public Location getPreyLocation(int i)
	{
		Player prey = null;
		
		prey = preyList.get(i);
		return prey.getLocation();
	}
}
