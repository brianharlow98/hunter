package com.harlowdev.Hunter.PlayerTypes;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class Predator 
{
	public ArrayList<Player> hunterList = new ArrayList<Player>();
	public ArrayList<Integer> hunterTarget = new ArrayList<Integer>();
	
	public boolean removeHunter(Player player)
	{
		int playerIndex = hunterList.indexOf(player);
		
		if(hunterList.contains(player))
		{
			hunterList.remove(playerIndex);
			hunterList.trimToSize();
			if(hunterList.contains(player))
			{
				return false;
			}

			return true;
		}

		return false;
	}
	
	public boolean clearHunter()
	{
		return hunterList.removeAll(hunterList);
	}
	
	public boolean listHunter(Player sender)
	{
		Player hunter = null;
		
		int size = hunterList.size();
		
		if(size > 0)
		{
			sender.sendMessage(ChatColor.GREEN 			+ "There are currently "
							 + ChatColor.LIGHT_PURPLE 	+ size + " "
							 + ChatColor.GREEN 			+ "hunters set!");
			
			for(int i = 0; i < size; i++)
			{
				hunter = hunterList.get(i);
				sender.sendMessage(ChatColor.GREEN + hunter.getName());
			}
			return true;
		}
		
		sender.sendMessage(ChatColor.RED + "There are no hunters set!");
		return false;
	}
	
	public Player getPlayer(int i)
	{
		return hunterList.get(i);
	}
}
