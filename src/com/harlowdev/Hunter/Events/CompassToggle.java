package com.harlowdev.Hunter.Events;

import org.bukkit.Material;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEvent;

import com.harlowdev.Hunter.PlayerTypes.Predator;
import com.harlowdev.Hunter.PlayerTypes.Prey;

public class CompassToggle implements Listener
{
	private Prey prey = null;
	private Predator hunter = null;
	
	public CompassToggle(Prey preyList, Predator hunterList)
	{
		prey = preyList;
		hunter = hunterList;
	}
	
	@EventHandler
	public void onToggle(PlayerInteractEvent e)
	{
		Player player = e.getPlayer();
		Action a = e.getAction();
		int playerIndex = hunter.hunterList.indexOf(player);
		int playerTarget = hunter.hunterTarget.get(playerIndex);
		int numPrey = prey.preyList.size();
		
		if((a.equals(Action.LEFT_CLICK_AIR) || a.equals(Action.LEFT_CLICK_BLOCK))
		 && player.getInventory().getItemInMainHand().getType().equals(Material.COMPASS))
		{
			if(numPrey > 1)
			{
				if(playerTarget < (numPrey - 1))
				{
					playerTarget++;
				}
				else
				{
					playerTarget = 0;
				}
				
				hunter.hunterTarget.set(playerIndex, playerTarget);
				player.setCompassTarget(prey.getPreyLocation(playerTarget));
				
				Player newTarget = prey.preyList.get(playerTarget);
				player.sendMessage(ChatColor.GREEN + "You are now tracking "
								 + ChatColor.LIGHT_PURPLE + newTarget.getName());
			}
			else
			{
				player.sendMessage(ChatColor.RED + "There are no other prey to track! You are still tracking "
								 + ChatColor.LIGHT_PURPLE + prey.preyList.get(playerIndex).getName());
			}
		}
		else if((a.equals(Action.RIGHT_CLICK_AIR) || a.equals(Action.RIGHT_CLICK_BLOCK)) 
			  && player.getInventory().getItemInMainHand().getType().equals(Material.COMPASS))
		{
			player.setCompassTarget(prey.getPreyLocation(playerTarget));
			Player target = prey.preyList.get(playerTarget);
			player.sendMessage(ChatColor.GREEN + "Compass updated with position of " + ChatColor.LIGHT_PURPLE + target.getName());
		}
	}
}
