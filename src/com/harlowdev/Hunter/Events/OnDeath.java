package com.harlowdev.Hunter.Events;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PlayerDeathEvent;

import com.harlowdev.Hunter.PlayerTypes.Prey;
import com.harlowdev.Hunter.PlayerTypes.Predator;
import com.harlowdev.Hunter.PlayerTypes.Spectator;

public class OnDeath implements Listener
{
	private Prey prey = null;
	//private Predator hunter = null;
	private Spectator spectator = null;
	
	public OnDeath(Prey preyList, Predator hunterList, Spectator specList)
	{
		prey = preyList;
		//hunter = hunterList;
		spectator = specList;
	}
	
	@EventHandler
	public void onDeath(PlayerDeathEvent e)
	{
		Player player = e.getEntity();
		boolean isPrey = prey.preyList.contains(player);
		
		if(isPrey)
		{
			prey.removePrey(player);
			spectator.specList.add(player);
			player.setGameMode(GameMode.SPECTATOR);
			player.sendMessage(ChatColor.GREEN + "You have been eliminated from the game! You are now a spectator and may watch.");
		}
	}
}
