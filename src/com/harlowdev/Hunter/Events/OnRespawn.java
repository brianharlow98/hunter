package com.harlowdev.Hunter.Events;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;
import org.bukkit.event.player.PlayerRespawnEvent;

import com.harlowdev.Hunter.PlayerTypes.Prey;
import com.harlowdev.Hunter.PlayerTypes.Predator;
import com.harlowdev.Hunter.PlayerTypes.Spectator;

public class OnRespawn implements Listener
{
	private Prey prey = null;
	private Predator hunter = null;
	//private Spectator spectator = null;
	
	public OnRespawn(Prey preyList, Predator hunterList, Spectator specList)
	{
		prey = preyList;
		hunter = hunterList;
		//spectator = specList;
	}
	
	@EventHandler
	public void onRespawn(PlayerRespawnEvent e)
	{
		Player player = e.getPlayer();
		boolean isHunter = hunter.hunterList.contains(player);
		
		if(isHunter)
		{
			player.getInventory().addItem(new ItemStack(Material.COMPASS));
			int playerIndex = hunter.hunterList.indexOf(player);
			int playerTarget = hunter.hunterTarget.get(playerIndex);
			player.setCompassTarget(prey.getPreyLocation(playerTarget));
			
			Player target = prey.preyList.get(playerTarget);
			player.sendMessage(ChatColor.GREEN + "You have received a tracker! You are currently tracking "
							 + ChatColor.LIGHT_PURPLE + target.getName());
		}
	}
}
